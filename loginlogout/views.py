from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect

# Create your views here.
response = {}

def index(request):
	html = 'loginhtml.html'
	return render(request,html,response)