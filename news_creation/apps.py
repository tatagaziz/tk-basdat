from django.apps import AppConfig


class NewsCreationConfig(AppConfig):
    name = 'news_creation'
