from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import RedirectView
from .views import index,indexPollBer,indexPollBi,tambahBerita, tambahPollingBerita, tambahPollingBiasa

urlpatterns = [
    path('', RedirectView.as_view(url='berita/')),
    path('berita/', index, name='berita'),
    path('post-berita/', tambahBerita),
    path('poll-berita/', indexPollBer),
    path('post-poll-berita/', tambahPollingBerita),
    path('poll-biasa/', indexPollBi),
    path('post-poll-biasa/', tambahPollingBiasa)
]
