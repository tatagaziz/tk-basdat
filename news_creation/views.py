from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

from .forms import TambahBerita, PollBerita, PollBiasa
from modelapp.models import Berita, PollingBerita as pollBeritaModel, PollingBiasa as pollBiasaModel, Polling, Universitas

# Create your views here.
response = {}
def index(request):
    return render(request,'news_creation/news_creation.html',response)
def indexPollBer(request):
    return render(request, 'news_creation/news_creation_pollBer.html',response)
def indexPollBi(request):
    return render(request, 'news_creation/news_creation_pollBi.html',response)

def tambahBerita(request):
    form = TambahBerita(request.POST)
    if form.is_valid():
        judul = form.cleaned_data['judul']
        url = form.cleaned_data['url']
        topik = form.cleaned_data['topik']
        jumlahKata = form.cleaned_data['jumlahKata']
        tag = form.cleaned_data['tag']
        id_universitas = Universitas.objects.get(id = 49)

        berita = Berita.objects.create(
            judul = judul, url = url, topik = topik, jumlah_kata = jumlahKata, tag = tag,
            id_universitas = id_universitas
        )

        return HttpResponseRedirect('/berita/')

def tambahPollingBerita(request):
    form = PollBerita(request.POST)
    if form.is_valid():
        urlBerita = form.cleaned_data['urlBerita']
        id = Polling.objects.get(1)

        polling = PollingBerita.objects.create(
            id_polling = id, url_berita = urlBerita
        )

        return HttpResponseRedirect('/berita/')

def tambahPollingBiasa(request):
    form = PollBiasa(request.POST)
    if form.is_valid():
        deskripsi = form.cleaned_data['deskripsi']
        url = 'something.com'
        id = Polling.objects.get(1)

        polling = PollingBiasa.objects.create(
            deskripsi = deskripsi, url = url, id_polling = id
        )

        return HttpResponseRedirect('/berita/')