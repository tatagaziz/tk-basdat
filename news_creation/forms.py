from django import forms

class TambahBerita(forms.Form):
    judul = forms.CharField(max_length=100)
    url = forms.CharField(max_length=100)
    topik = forms.CharField(max_length=100)
    jumlahKata = forms.IntegerField()
    tag = forms.CharField(max_length=50)

class PollBerita(forms.Form):
    urlBerita = forms.CharField(max_length=100)
    waktuMulai = forms.DateTimeField()
    waktuSelesai = forms.DateTimeField()
    pertanyaanSatu = forms.IntegerField()
    pertanyaanDua = forms.CharField(max_length=100)

class PollBiasa(forms.Form):
    deskripsi = forms.CharField(max_length=100)
    waktuMulai = forms.DateTimeField()
    waktuSelesai = forms.DateTimeField()
    pertanyaanSatu = forms.IntegerField()
    pertanyaanDua = forms.CharField(max_length=100)
